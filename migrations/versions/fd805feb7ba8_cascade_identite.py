"""Ajoute quelques cascades oubliées

Revision ID: fd805feb7ba8
Revises: 497ba81343f7
Create Date: 2023-10-25 18:27:13.222354

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "fd805feb7ba8"
down_revision = "497ba81343f7"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("admissions", schema=None) as batch_op:
        batch_op.drop_column("etudid")

    with op.batch_alter_table("identite", schema=None) as batch_op:
        batch_op.drop_constraint("admissions_etudid_fkey", type_="foreignkey")
        batch_op.create_foreign_key(
            "admissions_etudid_fkey",
            "admissions",
            ["admission_id"],
            ["id"],
            ondelete="CASCADE",
        )

    with op.batch_alter_table("apc_validation_rcue", schema=None) as batch_op:
        batch_op.drop_constraint("apc_validation_rcue_ue1_id_fkey", type_="foreignkey")
        batch_op.drop_constraint("apc_validation_rcue_ue2_id_fkey", type_="foreignkey")
        batch_op.create_foreign_key(
            "apc_validation_rcue_ue1_id_fkey",
            "notes_ue",
            ["ue1_id"],
            ["id"],
            ondelete="CASCADE",
        )
        batch_op.create_foreign_key(
            "apc_validation_rcue_ue2_id_fkey",
            "notes_ue",
            ["ue2_id"],
            ["id"],
            ondelete="CASCADE",
        )


def downgrade():
    with op.batch_alter_table("identite", schema=None) as batch_op:
        batch_op.drop_constraint("identite_dept_id_fkey", type_="foreignkey")
        batch_op.drop_constraint("admissions_etudid_fkey", type_="foreignkey")
        batch_op.create_foreign_key(
            "admissions_etudid_fkey", "admissions", ["admission_id"], ["id"]
        )

    with op.batch_alter_table("admissions", schema=None) as batch_op:
        batch_op.add_column(
            sa.Column("etudid", sa.INTEGER(), autoincrement=False, nullable=True)
        )

    with op.batch_alter_table("apc_validation_rcue", schema=None) as batch_op:
        batch_op.drop_constraint("apc_validation_rcue_ue1_id_fkey", type_="foreignkey")
        batch_op.drop_constraint("apc_validation_rcue_ue2_id_fkey", type_="foreignkey")
        batch_op.create_foreign_key(
            "apc_validation_rcue_ue2_id_fkey", "notes_ue", ["ue2_id"], ["id"]
        )
        batch_op.create_foreign_key(
            "apc_validation_rcue_ue1_id_fkey", "notes_ue", ["ue1_id"], ["id"]
        )
