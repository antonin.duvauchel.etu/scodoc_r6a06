"""formsemestre_not_nulls

Revision ID: 554c13cea377
Revises: 3c12f5850cff
Create Date: 2023-01-09 08:02:53.637488

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "554c13cea377"
down_revision = "3c12f5850cff"
branch_labels = None
depends_on = None


def upgrade():
    #
    op.execute("UPDATE notes_formsemestre SET titre = '' WHERE titre is NULL;")
    op.alter_column(
        "notes_formsemestre", "titre", existing_type=sa.TEXT(), nullable=False
    )
    op.execute(
        "UPDATE notes_formsemestre SET date_debut = now() WHERE date_debut is NULL;"
    )
    op.alter_column(
        "notes_formsemestre", "date_debut", existing_type=sa.DATE(), nullable=False
    )
    op.execute("UPDATE notes_formsemestre SET date_fin = now() WHERE date_fin is NULL;")
    op.alter_column(
        "notes_formsemestre", "date_fin", existing_type=sa.DATE(), nullable=False
    )
    op.execute(
        "UPDATE notes_formsemestre SET bul_bgcolor = 'white' WHERE bul_bgcolor is NULL;"
    )
    op.alter_column(
        "notes_formsemestre",
        "bul_bgcolor",
        existing_type=sa.VARCHAR(length=32),
        nullable=False,
        existing_server_default=sa.text("'white'::character varying"),
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column(
        "notes_formsemestre",
        "bul_bgcolor",
        existing_type=sa.VARCHAR(length=32),
        nullable=True,
        existing_server_default=sa.text("'white'::character varying"),
    )
    op.alter_column(
        "notes_formsemestre", "date_fin", existing_type=sa.DATE(), nullable=True
    )
    op.alter_column(
        "notes_formsemestre", "date_debut", existing_type=sa.DATE(), nullable=True
    )
    op.alter_column(
        "notes_formsemestre", "titre", existing_type=sa.TEXT(), nullable=True
    )
    # ### end Alembic commands ###
