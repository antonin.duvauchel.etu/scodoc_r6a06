"""cascades ref. comp.

Revision ID: d8288b7f0a3e
Revises: 5c7b208355df
Create Date: 2023-02-09 11:25:28.879434

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "d8288b7f0a3e"
down_revision = "5c7b208355df"
branch_labels = ""
depends_on = ""


def upgrade():
    # EVENTS
    op.drop_constraint(
        "scolar_events_formsemestre_id_fkey", "scolar_events", type_="foreignkey"
    )
    op.drop_constraint("scolar_events_ue_id_fkey", "scolar_events", type_="foreignkey")
    op.create_foreign_key(
        "scolar_events_ue_id_fkey",
        "scolar_events",
        "notes_ue",
        ["ue_id"],
        ["id"],
        ondelete="SET NULL",
    )
    op.create_foreign_key(
        "scolar_events_formsemestre_id_fkey",
        "scolar_events",
        "notes_formsemestre",
        ["formsemestre_id"],
        ["id"],
        ondelete="SET NULL",
    )
    # REF COMP
    op.drop_constraint(
        "apc_annee_parcours_parcours_id_fkey", "apc_annee_parcours", type_="foreignkey"
    )
    op.create_foreign_key(
        "apc_annee_parcours_parcours_id_fkey",
        "apc_annee_parcours",
        "apc_parcours",
        ["parcours_id"],
        ["id"],
        ondelete="CASCADE",
    )
    op.drop_constraint(
        "apc_app_critique_niveau_id_fkey", "apc_app_critique", type_="foreignkey"
    )
    op.create_foreign_key(
        "apc_app_critique_niveau_id_fkey",
        "apc_app_critique",
        "apc_niveau",
        ["niveau_id"],
        ["id"],
        ondelete="CASCADE",
    )
    op.drop_constraint(
        "apc_competence_referentiel_id_fkey", "apc_competence", type_="foreignkey"
    )
    op.create_foreign_key(
        "apc_competence_referentiel_id_fkey",
        "apc_competence",
        "apc_referentiel_competences",
        ["referentiel_id"],
        ["id"],
        ondelete="CASCADE",
    )
    op.drop_constraint(
        "apc_composante_essentielle_competence_id_fkey",
        "apc_composante_essentielle",
        type_="foreignkey",
    )
    op.create_foreign_key(
        "apc_composante_essentielle_competence_id_fkey",
        "apc_composante_essentielle",
        "apc_competence",
        ["competence_id"],
        ["id"],
        ondelete="CASCADE",
    )
    op.drop_constraint(
        "apc_modules_acs_app_crit_id_fkey", "apc_modules_acs", type_="foreignkey"
    )
    op.create_foreign_key(
        "apc_modules_acs_app_crit_id_fkey",
        "apc_modules_acs",
        "apc_app_critique",
        ["app_crit_id"],
        ["id"],
        ondelete="CASCADE",
    )
    op.drop_constraint(
        "apc_niveau_competence_id_fkey", "apc_niveau", type_="foreignkey"
    )
    op.create_foreign_key(
        "apc_niveau_competence_id_fkey",
        "apc_niveau",
        "apc_competence",
        ["competence_id"],
        ["id"],
        ondelete="CASCADE",
    )
    op.drop_constraint(
        "apc_parcours_referentiel_id_fkey", "apc_parcours", type_="foreignkey"
    )
    op.create_foreign_key(
        "apc_parcours_referentiel_id_fkey",
        "apc_parcours",
        "apc_referentiel_competences",
        ["referentiel_id"],
        ["id"],
        ondelete="CASCADE",
    )
    op.drop_constraint(
        "apc_referentiel_competences_dept_id_fkey",
        "apc_referentiel_competences",
        type_="foreignkey",
    )
    op.create_foreign_key(
        "apc_referentiel_competences_dept_id_fkey",
        "apc_referentiel_competences",
        "departement",
        ["dept_id"],
        ["id"],
        ondelete="CASCADE",
    )
    op.drop_constraint(
        "apc_situation_pro_competence_id_fkey", "apc_situation_pro", type_="foreignkey"
    )
    op.create_foreign_key(
        "apc_situation_pro_competence_id_fkey",
        "apc_situation_pro",
        "apc_competence",
        ["competence_id"],
        ["id"],
        ondelete="CASCADE",
    )
    op.drop_constraint(
        "apc_validation_rcue_parcours_id_fkey",
        "apc_validation_rcue",
        type_="foreignkey",
    )
    op.create_foreign_key(
        "apc_validation_rcue_parcours_id_fkey",
        "apc_validation_rcue",
        "apc_parcours",
        ["parcours_id"],
        ["id"],
        ondelete="set null",
    )
    op.drop_constraint(
        "notes_formations_referentiel_competence_id_fkey",
        "notes_formations",
        type_="foreignkey",
    )
    op.create_foreign_key(
        "notes_formations_referentiel_competence_id_fkey",
        "notes_formations",
        "apc_referentiel_competences",
        ["referentiel_competence_id"],
        ["id"],
        ondelete="SET NULL",
    )
    op.drop_constraint(
        "notes_formsemestre_inscription_parcour_id_fkey",
        "notes_formsemestre_inscription",
        type_="foreignkey",
    )
    op.create_foreign_key(
        "notes_formsemestre_inscription_parcour_id_fkey",
        "notes_formsemestre_inscription",
        "apc_parcours",
        ["parcour_id"],
        ["id"],
        ondelete="SET NULL",
    )
    op.drop_constraint(
        "notes_ue_niveau_competence_id_fkey", "notes_ue", type_="foreignkey"
    )
    op.drop_constraint("notes_ue_parcour_id_fkey", "notes_ue", type_="foreignkey")
    op.create_foreign_key(
        "notes_ue_niveau_competence_id_fkey",
        "notes_ue",
        "apc_niveau",
        ["niveau_competence_id"],
        ["id"],
        ondelete="SET NULL",
    )
    op.create_foreign_key(
        "notes_ue_parcour_id_fkey",
        "notes_ue",
        "apc_parcours",
        ["parcour_id"],
        ["id"],
        ondelete="SET NULL",
    )
    op.drop_constraint(
        "parcours_formsemestre_parcours_id_fkey",
        "parcours_formsemestre",
        type_="foreignkey",
    )
    op.create_foreign_key(
        "parcours_formsemestre_parcours_id_fkey",
        "parcours_formsemestre",
        "apc_parcours",
        ["parcours_id"],
        ["id"],
        ondelete="CASCADE",
    )
    op.drop_constraint(
        "parcours_modules_parcours_id_fkey", "parcours_modules", type_="foreignkey"
    )
    op.create_foreign_key(
        "parcours_modules_parcours_id_fkey",
        "parcours_modules",
        "apc_parcours",
        ["parcours_id"],
        ["id"],
        ondelete="CASCADE",
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(
        "parcours_modules_parcours_id_fkey", "parcours_modules", type_="foreignkey"
    )
    op.create_foreign_key(
        "parcours_modules_parcours_id_fkey",
        "parcours_modules",
        "apc_parcours",
        ["parcours_id"],
        ["id"],
    )
    op.drop_constraint(
        "parcours_formsemestre_parcours_id_fkey",
        "parcours_formsemestre",
        type_="foreignkey",
    )
    op.create_foreign_key(
        "parcours_formsemestre_parcours_id_fkey",
        "parcours_formsemestre",
        "apc_parcours",
        ["parcours_id"],
        ["id"],
    )
    op.drop_constraint("notes_ue_parcour_id_fkey", "notes_ue", type_="foreignkey")
    op.drop_constraint(
        "notes_ue_niveau_competence_id_fkey", "notes_ue", type_="foreignkey"
    )
    op.create_foreign_key(
        "notes_ue_parcour_id_fkey", "notes_ue", "apc_parcours", ["parcour_id"], ["id"]
    )
    op.create_foreign_key(
        "notes_ue_niveau_competence_id_fkey",
        "notes_ue",
        "apc_niveau",
        ["niveau_competence_id"],
        ["id"],
    )
    op.drop_constraint(
        "notes_formsemestre_inscription_parcour_id_fkey",
        "notes_formsemestre_inscription",
        type_="foreignkey",
    )
    op.create_foreign_key(
        "notes_formsemestre_inscription_parcour_id_fkey",
        "notes_formsemestre_inscription",
        "apc_parcours",
        ["parcour_id"],
        ["id"],
    )
    op.drop_constraint(
        "notes_formations_referentiel_competence_id_fkey",
        "notes_formations",
        type_="foreignkey",
    )
    op.create_foreign_key(
        "notes_formations_referentiel_competence_id_fkey",
        "notes_formations",
        "apc_referentiel_competences",
        ["referentiel_competence_id"],
        ["id"],
    )
    op.drop_constraint(
        "apc_validation_rcue_parcours_id_fkey",
        "apc_validation_rcue",
        type_="foreignkey",
    )
    op.create_foreign_key(
        "apc_validation_rcue_parcours_id_fkey",
        "apc_validation_rcue",
        "apc_parcours",
        ["parcours_id"],
        ["id"],
    )
    op.drop_constraint(
        "apc_situation_pro_competence_id_fkey", "apc_situation_pro", type_="foreignkey"
    )
    op.create_foreign_key(
        "apc_situation_pro_competence_id_fkey",
        "apc_situation_pro",
        "apc_competence",
        ["competence_id"],
        ["id"],
    )
    op.drop_constraint(
        "apc_referentiel_competences_dept_id_fkey",
        "apc_referentiel_competences",
        type_="foreignkey",
    )
    op.create_foreign_key(
        "apc_referentiel_competences_dept_id_fkey",
        "apc_referentiel_competences",
        "departement",
        ["dept_id"],
        ["id"],
    )
    op.drop_constraint(
        "apc_parcours_referentiel_id_fkey", "apc_parcours", type_="foreignkey"
    )
    op.create_foreign_key(
        "apc_parcours_referentiel_id_fkey",
        "apc_parcours",
        "apc_referentiel_competences",
        ["referentiel_id"],
        ["id"],
    )
    op.drop_constraint(
        "apc_niveau_competence_id_fkey", "apc_niveau", type_="foreignkey"
    )
    op.create_foreign_key(
        "apc_niveau_competence_id_fkey",
        "apc_niveau",
        "apc_competence",
        ["competence_id"],
        ["id"],
    )
    op.drop_constraint(
        "apc_modules_acs_app_crit_id_fkey", "apc_modules_acs", type_="foreignkey"
    )
    op.create_foreign_key(
        "apc_modules_acs_app_crit_id_fkey",
        "apc_modules_acs",
        "apc_app_critique",
        ["app_crit_id"],
        ["id"],
    )
    op.drop_constraint(
        "apc_composante_essentielle_competence_id_fkey",
        "apc_composante_essentielle",
        type_="foreignkey",
    )
    op.create_foreign_key(
        "apc_composante_essentielle_competence_id_fkey",
        "apc_composante_essentielle",
        "apc_competence",
        ["competence_id"],
        ["id"],
    )
    op.drop_constraint(
        "apc_competence_referentiel_id_fkey", "apc_competence", type_="foreignkey"
    )
    op.create_foreign_key(
        "apc_competence_referentiel_id_fkey",
        "apc_competence",
        "apc_referentiel_competences",
        ["referentiel_id"],
        ["id"],
    )
    op.drop_constraint(
        "apc_app_critique_niveau_id_fkey", "apc_app_critique", type_="foreignkey"
    )
    op.create_foreign_key(
        "apc_app_critique_niveau_id_fkey",
        "apc_app_critique",
        "apc_niveau",
        ["niveau_id"],
        ["id"],
    )
    op.drop_constraint(
        "apc_annee_parcours_parcours_id_fkey", "apc_annee_parcours", type_="foreignkey"
    )
    op.create_foreign_key(
        "apc_annee_parcours_parcours_id_fkey",
        "apc_annee_parcours",
        "apc_parcours",
        ["parcours_id"],
        ["id"],
    )
    # EVENTS
    op.drop_constraint("scolar_events_ue_id_fkey", "scolar_events", type_="foreignkey")
    op.drop_constraint(
        "scolar_events_formsemestre_id_fkey", "scolar_events", type_="foreignkey"
    )
    op.create_foreign_key(
        "scolar_events_ue_id_fkey", "scolar_events", "notes_ue", ["ue_id"], ["id"]
    )
    op.create_foreign_key(
        "scolar_events_formsemestre_id_fkey",
        "scolar_events",
        "notes_formsemestre",
        ["formsemestre_id"],
        ["id"],
    )

    # ### end Alembic commands ###
