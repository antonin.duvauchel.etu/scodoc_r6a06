"""evaluation date: modifie le codage des dates d'évaluations

Revision ID: 5c44d0d215ca
Revises: 45e0a855b8eb
Create Date: 2023-08-22 14:39:23.831483

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "5c44d0d215ca"
down_revision = "45e0a855b8eb"
branch_labels = None
depends_on = None


def upgrade():
    "modifie les colonnes codant les dates d'évaluations"
    with op.batch_alter_table("notes_evaluation", schema=None) as batch_op:
        batch_op.add_column(
            sa.Column("date_debut", sa.DateTime(timezone=True), nullable=True)
        )
        batch_op.add_column(
            sa.Column("date_fin", sa.DateTime(timezone=True), nullable=True)
        )
    # recode les dates existantes
    op.execute("UPDATE notes_evaluation SET date_debut = jour+heure_debut;")
    op.execute("UPDATE notes_evaluation SET date_fin = jour+heure_fin;")
    with op.batch_alter_table("notes_evaluation", schema=None) as batch_op:
        batch_op.drop_column("jour")
        batch_op.drop_column("heure_fin")
        batch_op.drop_column("heure_debut")


def downgrade():
    "modifie les colonnes codant les dates d'évaluations"
    with op.batch_alter_table("notes_evaluation", schema=None) as batch_op:
        batch_op.add_column(
            sa.Column(
                "heure_debut", postgresql.TIME(), autoincrement=False, nullable=True
            )
        )
        batch_op.add_column(
            sa.Column(
                "heure_fin", postgresql.TIME(), autoincrement=False, nullable=True
            )
        )
        batch_op.add_column(
            sa.Column("jour", sa.DATE(), autoincrement=False, nullable=True)
        )
    op.execute("UPDATE notes_evaluation SET jour = DATE(date_debut);")
    op.execute("UPDATE notes_evaluation SET heure_debut = date_debut::time;")
    op.execute("UPDATE notes_evaluation SET heure_fin = date_fin::time;")
    with op.batch_alter_table("notes_evaluation", schema=None) as batch_op:
        batch_op.drop_column("date_fin")
        batch_op.drop_column("date_debut")
