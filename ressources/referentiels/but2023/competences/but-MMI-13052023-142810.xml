<?xml version="1.0" encoding="UTF-8"?>
<referentiel_competence specialite="MMI"
                        specialite_long="Métiers du multimédia et de l&#039;Internet"
                        type="B.U.T." annexe="19"
                        type_structure="type2"
                        type_departement="secondaire"
                        version="2022-11-29 16:14:21"
>
    <competences>
                <competence nom_court="Comprendre"
                    numero="1"
                    libelle_long="Comprendre les écosystèmes, les besoins des utilisateurs et les dispositifs de communication numérique"
                    couleur="c1"
                    id="688548e4666873aa7a49491ba88a7271">
            <situations>
                                <situation>Prestataire : Les livrables constituent la première étape de la mission, qui se poursuit avec du conseil et de la production (agence de communication digitale, consultant...).</situation>
                                <situation>Organisation traditionnelle : Les livrables sont produits par les services de l’organisation qui peut être une entreprise, une administration ou une association.</situation>
                                <situation>Start-up : Les livrables sont utilisés de façon itérative pour améliorer le produit créé par l’organisation, amélioration pilotée par un product owner.</situation>
                            </situations>
            <composantes_essentielles>
                                <composante>en intégrant les enjeux humains, écologiques et éthiques</composante>
                                <composante>en écoutant et observant les utilisateurs</composante>
                                <composante>en s’appuyant sur des données quantitatives pertinentes et des outils statistiques adaptés</composante>
                                <composante>en sollicitant des modèles théoriques issus des sciences humaines et sociales</composante>
                                <composante>en restituant les résultats de manière synthétique</composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Comprendre les éléments de communication et les attentes utilisateurs" annee="BUT1">
                <acs>
                                        <ac code="AC11.01">Présenter une organisation, ses activités et son environnement (économique, sociologique, culturel, juridique, technologique, communicationnel et médiatique)</ac>
                                        <ac code="AC11.02">Évaluer un site web, un produit multimédia ou un dispositif interactif existant en s’appuyant sur des guides de bonnes pratiques</ac>
                                        <ac code="AC11.03">Produire des analyses statistiques descriptives et les interpréter pour évaluer un contexte socio-économique</ac>
                                        <ac code="AC11.04">Analyser des formes médiatiques et leur sémiotique</ac>
                                        <ac code="AC11.05">Identifier les cibles (critères socio-économiques, démographiques, géographiques, culturels...)</ac>
                                        <ac code="AC11.06">Réaliser des entretiens utilisateurs pour construire des personae et des récits utilisateurs (user stories)</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="Comprendre la stratégie de communication et l’expérience utilisateur" annee="BUT2">
                <acs>
                                        <ac code="AC21.01">Analyser la stratégie de communication ou marketing d’un acteur, d’une organisation au regard d’un secteur ou d’un marché (stratégie, mission, valeurs...)</ac>
                                        <ac code="AC21.02">Auditer un site web, une marque ou un service, en termes de trafic et de référencement</ac>
                                        <ac code="AC21.03">Traiter des données avec des outils statistiques pour faciliter leur analyse et leur exploitation</ac>
                                        <ac code="AC21.04">Identifier et décrire les parcours client à partir d’enquêtes de terrain</ac>
                                        <ac code="AC21.05">Cartographier les expériences utilisateur : points de contact, points de friction et de satisfaction, carte d’empathie.</ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
                <competence nom_court="Concevoir"
                    numero="2"
                    libelle_long="Concevoir ou co-concevoir une réponse stratégique pertinente à une problématique complexe"
                    couleur="c2"
                    id="786d957f2fd89908751ca0ea0a835a37">
            <situations>
                                <situation>Prestataire : Livrables sous forme de recommandation stratégique, dans une approche de communication ou de conception de produit / service.</situation>
                                <situation>Organisation traditionnelle : Direction marketing, communication, DPO, CRM… dans une entreprise, une administration ou une association.</situation>
                                <situation>Start-up : Les livrables sont utilisés de façon itérative pour améliorer le produit créé par l’organisation, amélioration pilotée par un product owner.</situation>
                            </situations>
            <composantes_essentielles>
                                <composante>en optimisant la responsabilité sociale et environnementale de l’organisation</composante>
                                <composante>en s’intégrant aux écosystèmes physiques et numériques des parties prenantes</composante>
                                <composante>en s’appuyant sur les usages et les modes de communication observés</composante>
                                <composante>en enrichissant sa démarche de connaissances sociologiques, esthétiques, culturelles et inter-culturelles</composante>
                                <composante>en présentant de façon convaincante la réponse proposée, en français, en anglais ou dans d’autres langues</composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Concevoir une réponse stratégique" annee="BUT1">
                <acs>
                                        <ac code="AC12.01">Concevoir un produit ou un service en terme d’usage et de fonctionnalité</ac>
                                        <ac code="AC12.02">Construire la proposition de valeur d’un produit ou d’un service</ac>
                                        <ac code="AC12.03">Proposer une recommandation marketing (cibles, objectifs, points de contact)</ac>
                                        <ac code="AC12.04">Proposer une stratégie de communication</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="Co-concevoir une réponse stratégique" annee="BUT2">
                <acs>
                                        <ac code="AC22.01">Co-concevoir un produit ou un service (proposition de valeur, fonctionnalités...)</ac>
                                        <ac code="AC22.02">Produire une recommandation ergonomique à partir des tests utilisateurs (sur système fonctionnel, prototype ou maquette interactive)</ac>
                                        <ac code="AC22.03">Co-construire une recommandation stratégique (en structurant un plan d’action)</ac>
                                        <ac code="AC22.04">Optimiser le référencement d’un site web, d’un produit ou d’un service</ac>
                                        <ac code="AC22.05">Mettre en place une présence sur les réseaux sociaux</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="3" libelle="Co-concevoir une réponse stratégique complexe et prospective" annee="BUT3">
                <acs>
                                        <ac code="AC32.01">Projeter les futurs possibles d’une organisation, d’une marque, d’un service, produit ou secteur (tendances, évolutions, scénarios prospectifs)</ac>
                                        <ac code="AC32.02">Co-construire un produit ou service de manière itérative (ateliers de créativité, idéation, définition de l’expérience utilisateur, exploitation des résultats de test)</ac>
                                        <ac code="AC32.03">Concevoir et mettre en oeuvre une communication 360, plurimédia ou transmédia</ac>
                                        <ac code="AC32.04">Construire des outils de validation et de suivi (flux, indicateurs de performance, tableaux de bord, référencement, engagement...)</ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
                <competence nom_court="Exprimer"
                    numero="3"
                    libelle_long="Exprimer un message avec les médias numériques pour informer et communiquer"
                    couleur="c3"
                    id="6a5a25b106187717ff27758e789dbde8">
            <situations>
                                <situation>Prestataire : Création et réalisation de contenu multimédia dans une agence généraliste ou spécialisée (agence de communication digitale, studio graphique, studio digital, production de jeu vidéo, production vidéo…).</situation>
                                <situation>Organisation traditionnelle : Service intégré dans une entreprise, une administration ou une association : production interne et gestion des utilisateurs.</situation>
                                <situation>Start-up : Itérations continues : le discours s’adapte aux évolutions de l’entreprise, de son marché et de sa communauté.</situation>
                            </situations>
            <composantes_essentielles>
                                <composante>en veillant à la qualité esthétique des créations et en la justifiant par des références culturelles et artistiques</composante>
                                <composante>en produisant un discours de qualité, appuyé sur les théories du récit et les traditions narratives</composante>
                                <composante>en respectant la stratégie de communication établie</composante>
                                <composante>en veillant à la qualité orthographique, grammaticale et typographique des productions</composante>
                                <composante>en communiquant en français, en anglais ou dans d’autres langues</composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Exprimer un message par des productions simples" annee="BUT1">
                <acs>
                                        <ac code="AC13.01">Ecrire pour les médias numériques</ac>
                                        <ac code="AC13.02">Produire des pistes graphiques et des planches d’inspiration</ac>
                                        <ac code="AC13.03">Créer, composer et retoucher des visuels</ac>
                                        <ac code="AC13.04">Tourner et monter une vidéo (scénario, captation image et son...)</ac>
                                        <ac code="AC13.05">Designer une interface web (wireframes, UI)</ac>
                                        <ac code="AC13.06">Optimiser les médias en fonction de leurs usages et supports de diffusion</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="Exprimer une identité visuelle et éditoriale transmédia" annee="BUT2">
                <acs>
                                        <ac code="AC23.01">Produire un écrit journalistique sourcé et documenté</ac>
                                        <ac code="AC23.02">Définir une iconographie (illustrations, photographies, vidéos)</ac>
                                        <ac code="AC23.03">Créer et décliner une identité visuelle (charte graphique)</ac>
                                        <ac code="AC23.04">Imaginer, écrire et scénariser en vue d’une communication multimédia ou transmédia</ac>
                                        <ac code="AC23.05">Réaliser, composer et produire pour une communication plurimédia</ac>
                                        <ac code="AC23.06">Élaborer et produire des animations, des designs sonores, des effets spéciaux, de la visualisation de données ou de la 3D</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="3" libelle="Exprimer un récit interactif et une direction artistique" annee="BUT3">
                <acs>
                                        <ac code="AC33.01">Adopter et justifier une démarche originale et personnelle dans ses productions</ac>
                                        <ac code="AC33.02">Concevoir un design system et en produire les éléments visuels, graphiques ou sonores</ac>
                                        <ac code="AC33.03">Maitriser les étapes de production d&#039;un projet multimédia</ac>
                                        <ac code="AC33.04">Produire les éléments pour une expérience sophistiquée (notamment immersive, en réalité virtuelle, augmentée...)</ac>
                                        <ac code="AC33.05">Apprehender les enjeux liés à la direction artistique</ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
                <competence nom_court="Développer"
                    numero="4"
                    libelle_long="Développer pour le web et les médias numériques"
                    couleur="c4"
                    id="bdeb25b7e21ebc4dc5f3c0f97db7285e">
            <situations>
                                <situation>Petite structure : Développement à partir de 0, avec une grande liberté en termes d’outils, de technologies, de méthodologie et de décision.</situation>
                                <situation>Grande organisation : Développement dans une grande organisation en respectant des processus structurés et contraignants.</situation>
                                <situation>Maintenance logicielle : Support, maintenance corrective et évolutive, gestion de la dette technique.</situation>
                            </situations>
            <composantes_essentielles>
                                <composante>en se conformant aux standards du Web et aux normes d’accessibilité</composante>
                                <composante>en s’appuyant sur des concepts théoriques issus de l’informatique et des sciences de l’information</composante>
                                <composante>en produisant du code fonctionnel, sobre et réutilisable</composante>
                                <composante>en utilisant les outils favorisant un développement itératif et collaboratif</composante>
                                <composante>en veillant à la sécurité des systèmes et des données</composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Développer un site web simple et le met en ligne" annee="BUT1">
                <acs>
                                        <ac code="AC14.01">Exploiter de manière autonome un environnement de développement efficace et productif</ac>
                                        <ac code="AC14.02">Produire des pages Web fluides incluant un balisage sémantique efficace et des interactions simples</ac>
                                        <ac code="AC14.03">Générer des pages Web à partir de données structurées</ac>
                                        <ac code="AC14.04">Mettre en ligne une application Web en utilisant une solution d’hébergement standard</ac>
                                        <ac code="AC14.05">Modéliser les données d’une application Web</ac>
                                        <ac code="AC14.06">Déployer et personnaliser une application Web en utilisant un CMS ou un framework MVC</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="Développer une application Web interactive" annee="BUT2">
                <acs>
                                        <ac code="AC24.01">Produire des pages et applications Web responsives</ac>
                                        <ac code="AC24.02">Mettre en place ou développer un back office</ac>
                                        <ac code="AC24.03">Intégrer, produire ou développer des interactions riches ou des dispositifs interactifs</ac>
                                        <ac code="AC24.04">Modéliser les traitements d’une application Web</ac>
                                        <ac code="AC24.05">Optimiser une application web en termes de référencement et de temps de chargement</ac>
                                        <ac code="AC24.06">Configurer une solution d’hébergement adaptée aux besoins</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="3" libelle="Développer un écosystème numérique complexe" annee="BUT3">
                <acs>
                                        <ac code="AC34.01">Développer à l’aide d’un framework de développement côté client</ac>
                                        <ac code="AC34.02">Développer à l’aide d’un framework de développement côté serveur</ac>
                                        <ac code="AC34.03">Développer des dispositifs interactifs sophistiqués</ac>
                                        <ac code="AC34.04">Concevoir et développer des composants logiciels, plugins ou extensions</ac>
                                        <ac code="AC34.05">Maitriser l&#039;hébergement et le déploiement d&#039;applications</ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
                <competence nom_court="Entreprendre"
                    numero="5"
                    libelle_long="Entreprendre dans le secteur du numérique"
                    couleur="c5"
                    id="689e945289af4e30650a910291c38e8a">
            <situations>
                                <situation>Prestataire : Gestion de projet classique pour un client, dans une agence généraliste ou spécialisée (studio graphique, studio digital, production de jeu vidéo, production vidéo…).</situation>
                                <situation>Organisation traditionnelle : Accompagnement du changement et intrapreneuriat dans une entreprise, une administration ou une association.</situation>
                                <situation>Start-up: Entrepreneuriat et gestion de projet avec une méthode d’amélioration continue.</situation>
                            </situations>
            <composantes_essentielles>
                                <composante>en s’appuyant sur une veille technologique et des modèles de l’innovation</composante>
                                <composante>en favorisant la collaboration entre les parties prenantes du projet</composante>
                                <composante>en respectant le droit et la vie privée</composante>
                                <composante>en favorisant la sobriété numérique</composante>
                                <composante>en exploitant des cadres de réflexion français et internationaux</composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Entreprendre un projet numérique" annee="BUT1">
                <acs>
                                        <ac code="AC15.01">Gérer un projet avec une méthode classique</ac>
                                        <ac code="AC15.02">Budgéter un projet et suivre sa rentabilité</ac>
                                        <ac code="AC15.03">Découvrir les écosystèmes d’innovation numérique (fab labs, living labs, tiers-lieux, incubateurs…)</ac>
                                        <ac code="AC15.04">Analyser un produit ou un service innovant en identifiant les propositions de valeurs et en évaluant les solutions proposées</ac>
                                        <ac code="AC15.05">Construire une présence en ligne professionnelle (personal branding)</ac>
                                        <ac code="AC15.06">Interagir au sein des organisations</ac>
                                        <ac code="AC15.07">Produire un message écrit ou oral professionnel</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="Entreprendre un projet au sein d’un écosystème numérique" annee="BUT2">
                <acs>
                                        <ac code="AC25.01">Gérer un projet avec une méthode d’amélioration continue par exemple une méthode agile</ac>
                                        <ac code="AC25.02">Cartographier un écosystème (identification des acteurs, synthèse des propositions de valeur)</ac>
                                        <ac code="AC25.03">Initier la constitution d’un réseau professionnel</ac>
                                        <ac code="AC25.04">Collaborer au sein des organisations</ac>
                                        <ac code="AC25.05">Maitriser les codes des productions écrites et orales professionnelles</ac>
                                        <ac code="AC25.06">Prendre en compte les contraintes juridiques</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="3" libelle="Entreprendre dans le numérique" annee="BUT3">
                <acs>
                                        <ac code="AC35.01">Piloter un produit, un service ou une équipe</ac>
                                        <ac code="AC35.02">Maîtriser la qualité en projet Web ou multimédia</ac>
                                        <ac code="AC35.03">Concevoir un projet d’entreprise innovante en définissant le nom, l’identité, la forme juridique et le ton de la marque</ac>
                                        <ac code="AC35.04">Défendre un projet de manière convaincante</ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
            </competences>
    <parcours>
                <parcour numero="0" libelle="Création Numérique" code="Crea">
                        <annee ordre="1">
                                <competence niveau="1" id="688548e4666873aa7a49491ba88a7271"/>
                                <competence niveau="1" id="786d957f2fd89908751ca0ea0a835a37"/>
                                <competence niveau="1" id="6a5a25b106187717ff27758e789dbde8"/>
                                <competence niveau="1" id="bdeb25b7e21ebc4dc5f3c0f97db7285e"/>
                                <competence niveau="1" id="689e945289af4e30650a910291c38e8a"/>
                            </annee>
                        <annee ordre="2">
                                <competence niveau="2" id="688548e4666873aa7a49491ba88a7271"/>
                                <competence niveau="2" id="786d957f2fd89908751ca0ea0a835a37"/>
                                <competence niveau="2" id="6a5a25b106187717ff27758e789dbde8"/>
                                <competence niveau="2" id="bdeb25b7e21ebc4dc5f3c0f97db7285e"/>
                                <competence niveau="2" id="689e945289af4e30650a910291c38e8a"/>
                            </annee>
                        <annee ordre="3">
                                <competence niveau="3" id="6a5a25b106187717ff27758e789dbde8"/>
                                <competence niveau="3" id="689e945289af4e30650a910291c38e8a"/>
                            </annee>
                    </parcour>
                <parcour numero="0" libelle="Stratégie de communication numérique et design d’expérience" code="Strat-UX">
                        <annee ordre="1">
                                <competence niveau="1" id="688548e4666873aa7a49491ba88a7271"/>
                                <competence niveau="1" id="786d957f2fd89908751ca0ea0a835a37"/>
                                <competence niveau="1" id="689e945289af4e30650a910291c38e8a"/>
                                <competence niveau="1" id="bdeb25b7e21ebc4dc5f3c0f97db7285e"/>
                                <competence niveau="1" id="6a5a25b106187717ff27758e789dbde8"/>
                            </annee>
                        <annee ordre="2">
                                <competence niveau="2" id="688548e4666873aa7a49491ba88a7271"/>
                                <competence niveau="2" id="786d957f2fd89908751ca0ea0a835a37"/>
                                <competence niveau="2" id="6a5a25b106187717ff27758e789dbde8"/>
                                <competence niveau="2" id="bdeb25b7e21ebc4dc5f3c0f97db7285e"/>
                                <competence niveau="2" id="689e945289af4e30650a910291c38e8a"/>
                            </annee>
                        <annee ordre="3">
                                <competence niveau="3" id="786d957f2fd89908751ca0ea0a835a37"/>
                                <competence niveau="3" id="689e945289af4e30650a910291c38e8a"/>
                            </annee>
                    </parcour>
                <parcour numero="0" libelle="Développement web et dispositifs interactifs" code="DWeb-DI">
                        <annee ordre="1">
                                <competence niveau="1" id="688548e4666873aa7a49491ba88a7271"/>
                                <competence niveau="1" id="786d957f2fd89908751ca0ea0a835a37"/>
                                <competence niveau="1" id="6a5a25b106187717ff27758e789dbde8"/>
                                <competence niveau="1" id="bdeb25b7e21ebc4dc5f3c0f97db7285e"/>
                                <competence niveau="1" id="689e945289af4e30650a910291c38e8a"/>
                            </annee>
                        <annee ordre="2">
                                <competence niveau="2" id="689e945289af4e30650a910291c38e8a"/>
                                <competence niveau="2" id="bdeb25b7e21ebc4dc5f3c0f97db7285e"/>
                                <competence niveau="2" id="6a5a25b106187717ff27758e789dbde8"/>
                                <competence niveau="2" id="786d957f2fd89908751ca0ea0a835a37"/>
                                <competence niveau="2" id="688548e4666873aa7a49491ba88a7271"/>
                            </annee>
                        <annee ordre="3">
                                <competence niveau="3" id="bdeb25b7e21ebc4dc5f3c0f97db7285e"/>
                                <competence niveau="3" id="689e945289af4e30650a910291c38e8a"/>
                            </annee>
                    </parcour>
            </parcours>
</referentiel_competence>
