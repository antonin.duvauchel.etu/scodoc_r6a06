# -*- mode: python -*-
# -*- coding: utf-8 -*-

""" Test création/accès/clonage formsemestre
"""
from flask import Response
import pytest
from tests.unit import yaml_setup, call_view

import app
from app.models import Formation, FormSemestre
from app.scodoc import (
    sco_archives_formsemestre,
    sco_cost_formation,
    sco_debouche,
    sco_edit_ue,
    sco_evaluations,
    sco_evaluation_check_abs,
    sco_evaluation_recap,
    sco_formation_versions,
    sco_formsemestre_edit,
    sco_formsemestre_inscriptions,
    sco_formsemestre_status,
    sco_groups,
    sco_inscr_passage,
    sco_lycee,
    sco_moduleimpl_inscriptions,
    sco_poursuite_dut,
    sco_preferences,
    sco_recapcomplet,
    sco_report,
    sco_undo_notes,
)

from app.scodoc import sco_utils as scu
from app.views import notes, scolar
from config import TestConfig

DEPT = TestConfig.DEPT_TEST


def test_formsemestres_associate_new_version(test_client):
    """Test association à une nouvelle version du programme"""
    app.set_sco_dept(DEPT)
    # Construit la base de test GB une seule fois
    # puis lance les tests de jury
    doc, formation, formsemestre_titres = yaml_setup.setup_from_yaml(
        "tests/ressources/yaml/simple_formsemestres.yaml"
    )
    for formsemestre_titre in formsemestre_titres:
        formsemestre = yaml_setup.create_formsemestre_with_etuds(
            doc, formation, formsemestre_titre
        )
        assert formsemestre
    formation_geii = Formation.query.filter_by(acronyme="BUT GEII", version=1).first()
    assert formation_geii.id == formation.id
    formsemestres = formation.formsemestres.all()
    assert len(formsemestres) == len(formsemestre_titres)
    # On a deux S1:
    assert len(formsemestres) == 2
    assert {s.semestre_id for s in formsemestres} == {1}
    # Les rattache à une nouvelle version de la formation:
    formsemestre_ids = [s.id for s in formsemestres]
    sco_formation_versions.do_formsemestres_associate_new_version(
        formation.id, formsemestre_ids
    )
    new_formation: Formation = Formation.query.filter_by(
        acronyme="BUT GEII", version=2
    ).first()
    assert new_formation
    assert formsemestres[0].formation_id == new_formation.id
    assert formsemestres[1].formation_id == new_formation.id


def test_formsemestre_misc_views(test_client):
    """Test de nombreuses vues: test juste que la page ne plante,
    pas de vérif du contenu ni de soumission si c'est un formulaire.

    Note: les anciennes vues renvoient souvent des str au lieu de Response.
    """
    app.set_sco_dept(DEPT)
    doc, formation, formsemestre_titres = yaml_setup.setup_from_yaml(
        "tests/ressources/yaml/simple_formsemestres.yaml"
    )
    for formsemestre_titre in formsemestre_titres:
        formsemestre = yaml_setup.create_formsemestre_with_etuds(
            doc, formation, formsemestre_titre
        )
        assert formsemestre
    formsemestre: FormSemestre = FormSemestre.query.first()

    # ----- MENU SEMESTRE
    _ = sco_formsemestre_status.formsemestre_status(formsemestre_id=formsemestre.id)
    _ = sco_edit_ue.ue_table(formsemestre.formation_id)
    _ = sco_formsemestre_edit.formsemestre_editwithmodules(formsemestre.id)
    _ = sco_preferences.SemPreferences(formsemestre_id=formsemestre.id).edit()
    _ = sco_formsemestre_edit.formsemestre_edit_options(formsemestre.id)
    assert formsemestre.etat
    formsemestre.flip_lock()
    assert not formsemestre.etat
    formsemestre.flip_lock()
    assert formsemestre.etat
    ans = sco_formsemestre_status.formsemestre_description(
        formsemestre.id, with_evals=True
    )
    assert isinstance(ans, (str, Response))  # ici c'est une str
    ans = sco_formsemestre_status.formsemestre_description(
        formsemestre.id, with_evals=True, fmt="xls"
    )
    assert isinstance(ans, Response)
    assert ans.status == "200 OK"
    assert ans.mimetype == scu.XLSX_MIMETYPE
    ans = sco_formsemestre_status.formsemestre_description(
        formsemestre.id, with_evals=True, fmt="pdf"
    )
    assert isinstance(ans, Response)
    assert ans.status == "200 OK"
    assert ans.mimetype == scu.PDF_MIMETYPE
    ans = sco_evaluation_check_abs.formsemestre_check_absences_html(formsemestre.id)
    assert isinstance(ans, (str, Response))
    # Appel direct de la vue: enlève les décorateurs:
    ans = call_view(notes.formsemestre_enseignants_list, formsemestre.id)
    assert isinstance(ans, (str, Response))  # ici str
    # Juste la page dialogue avant opération::
    ans = sco_formsemestre_edit.formsemestre_clone(formsemestre.id)
    ans = sco_formation_versions.formsemestre_associate_new_version(
        formsemestre.formation_id, formsemestre.id
    )
    ans = sco_formsemestre_edit.formsemestre_delete(formsemestre.id)

    # ----- MENU INSCRIPTIONS
    ans = sco_moduleimpl_inscriptions.moduleimpl_inscriptions_stats(formsemestre.id)
    ans = sco_inscr_passage.formsemestre_inscr_passage(formsemestre.id)
    ans = call_view(scolar.form_students_import_excel, formsemestre.id)
    assert isinstance(ans, str)
    ans = call_view(scolar.form_students_import_infos_admissions, formsemestre.id)
    ans = sco_formsemestre_inscriptions.formsemestre_inscrits_ailleurs(formsemestre.id)

    # ----- MENU GROUPES
    ans = call_view(scolar.groups_view, formsemestre.id)
    ans = call_view(scolar.partition_editor, formsemestre.id)
    ans = sco_groups.edit_partition_form(formsemestre.id)

    # ----- MENU NOTES
    ans = sco_recapcomplet.formsemestre_recapcomplet(formsemestre.id)
    ans = sco_recapcomplet.formsemestre_recapcomplet(formsemestre.id, tabformat="evals")
    ans = sco_recapcomplet.formsemestre_recapcomplet(formsemestre.id, tabformat="xlsx")
    assert isinstance(ans, Response)
    assert ans.status == "200 OK"
    assert ans.mimetype == scu.XLSX_MIMETYPE
    ans = sco_recapcomplet.formsemestre_recapcomplet(formsemestre.id, tabformat="json")
    assert isinstance(ans, Response)
    assert ans.status == "200 OK"
    assert ans.mimetype == scu.JSON_MIMETYPE
    ans = sco_evaluation_recap.evaluations_recap(formsemestre.id)

    # Bulletins pdf:
    ans = call_view(notes.formsemestre_bulletins_pdf_choice, formsemestre.id)
    assert isinstance(ans, str)  # sans version, on a le formulaire de choix, une str
    ans = call_view(
        notes.formsemestre_bulletins_pdf_choice, formsemestre.id, version="long"
    )
    assert isinstance(ans, Response)
    assert ans.status == "200 OK"
    assert ans.mimetype == scu.PDF_MIMETYPE
    # on ne teste pas les mails :)
    ans = sco_evaluations.formsemestre_evaluations_cal(formsemestre.id)
    assert isinstance(ans, str)
    sco_undo_notes.formsemestre_list_saisies_notes(formsemestre.id)

    # ----- MENU JURY
    ans = sco_recapcomplet.formsemestre_recapcomplet(formsemestre.id, mode_jury=True)
    ans = sco_recapcomplet.formsemestre_recapcomplet(
        formsemestre.id, mode_jury=True, tabformat="xlsx"
    )
    assert isinstance(ans, Response)
    assert ans.status == "200 OK"
    assert ans.mimetype == scu.XLSX_MIMETYPE
    ans = sco_recapcomplet.formsemestre_recapcomplet(
        formsemestre.id, mode_jury=True, tabformat="json"
    )
    assert isinstance(ans, Response)
    assert ans.status == "200 OK"
    assert ans.mimetype == scu.JSON_MIMETYPE
    ans = sco_archives_formsemestre.formsemestre_archive(formsemestre.id)
    ans = sco_archives_formsemestre.formsemestre_list_archives(formsemestre.id)

    # ----- MENU STATISTIQUES
    ans = sco_report.formsemestre_report_counts(formsemestre.id)
    ans = sco_report.formsemestre_report_counts(formsemestre.id, fmt="xls")
    assert isinstance(ans, Response)
    assert ans.status == "200 OK"
    assert ans.mimetype == scu.XLSX_MIMETYPE
    ans = sco_report.formsemestre_suivi_cohorte(formsemestre.id)
    ans = sco_report.formsemestre_suivi_cohorte(formsemestre.id, fmt="pdf")
    assert isinstance(ans, Response)
    assert ans.status == "200 OK"
    assert ans.mimetype == scu.PDF_MIMETYPE
    ans = sco_report.formsemestre_graph_cursus(formsemestre.id)
    ans = sco_report.formsemestre_suivi_cursus(formsemestre.id)
    ans = sco_lycee.formsemestre_etuds_lycees(formsemestre.id)
    ans = sco_poursuite_dut.formsemestre_poursuite_report(formsemestre.id)
    # pas de test des avis de poursuite
    ans = sco_debouche.report_debouche_date(start_year=2000)
    ans = sco_cost_formation.formsemestre_estim_cost(formsemestre.id)
    # pas de test des indicateurs de suivi BUT
